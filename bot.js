console.log("STARTING");
console.log("--------------------------------------------------");

var Discord = require('discord.io');
var auth = require('./auth.json');
var fs = require('fs');

var cmdsFile = require('./commands.js');

console.log("Requires loaded in.");

var commands = cmdsFile.cmds;
var admins = cmdsFile.admins;
console.log(commands.length + " Commands loaded");

var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

console.log("initializing...");

var initialized = false;
bot.commands = commands;

bot.data = null;
bot.lastSave = Date.now();

bot.saveBackup = function (silent = false)
{
    console.log("Writing Backup");
    fs.writeFile("./botDataBackup.json", JSON.stringify(bot.data), function (err)
    {
        if (err)
        {
            console.log(err);
            bot.sendMessage({
                to: bot.data.lastServer,
                message: "Failed to create a back-up.",
                typing: false
            });
        }
        else
        {
            if (!silent)
            {
                bot.sendMessage({
                    to: bot.data.lastServer,
                    message: "Bot data back-up created.",
                    typing: false
                });
            }
        }
    });
};

bot.saveData = function (callback, context)
{
    console.log("Saving...");
    try
    {
        if (bot.data != null && bot.data != {} && initialized)
        {
            bot.data = bot.data || {};

            fs.writeFile("./botData.json", JSON.stringify(bot.data), function (err)
            {
                if (err)
                {
                    console.log(err);
                }

                context = context || this;
                callback && callback.call(context, true);
            });
        }
    }
    catch (ex)
    {
        console.log("Error while saving!");
        console.log(ex);
        context = context || this;
        callback && callback.call(context, false);
    }
};

console.log("savedata function defined");

bot.on('ready', function (evt)
{
    console.log('Connected!');
    console.log('Logged in as: ');
    console.log(bot.username + ' - (' + bot.id + ')');

    fs.readFile('./botData.json', function read (err, data) 
    {
        if (err) 
        {
            data = "{}";
        }
        try
        {
            bot.data = JSON.parse(data);
        }
        catch (e)
        {
            if (bot.data == null)
            {
                console.log("Memory broken, will attempt to restore backup.");
            }
        }

        if (bot.data == null || bot.data == {})
        {
            fs.readFile('./botDataBackup.json', function read (err, data)
            {
                if (err)
                {
                    data = "{}";
                }
                try
                {
                    bot.data = JSON.parse(data);
                    console.log("Backup restored");

                    if (bot.data.lastServer)
                    {
                        bot.sendMessage({
                            to: bot.data.lastServer,
                            message: "A crash occurred, but I managed to restore a backup of my memory.",
                            typing: false
                        });
                        saveBackup = false;
                    }
                }
                catch (e)
                {
                    if (bot.data == null)
                    {
                        bot.data = {};
                        console.log("Memory backup broken, making new");
                        saveBackup = false;

                        if (bot.data.lastServer)
                        {
                            bot.sendMessage({
                                to: bot.data.lastServer,
                                message: "A crash occurred, and my memory file was lost :(",
                                typing: false
                            });
                        }
                    }
                }
                initialized = true;
            });
        }
        else //successfully loaded
        {
            bot.saveBackup(true);

            initialized = true;
        }

        bot.data = bot.data || {};

        bot.data.servers = bot.data.servers || [];

        for (var i = 0; i < bot.data.servers.length; i++)
        {
            bot.data[bot.data.servers[i]].adventurelock = [];
        }

        console.log("Checking if I need to send update message...");

        if (bot.data.update && bot.data.update != "no")
        {
            bot.sendMessage({
                to: bot.data.update,
                message: "Updated!",
                typing: false
            });

            bot.data.update = "no";
        }
        else
        {
            console.log("no update notifiy to do");
        }
    });
});

console.log("onready defined");

bot.on('message', function (user, userID, channelID, message, evt)
{
    if (userID == "1034390292570308658")
        return;

    console.log(message);
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`

    if (!initialized)
        return;

    var wasBot = evt.d.author.bot;

    var id = evt.d.guild_id || userID;

    if (bot.data.servers.indexOf(id) < 0)
    {
        bot.data.servers.push(id);
    }

    var group = [userID];

    var words = message.split(' ');
    bot.data[id] = bot.data[id] || {};

    if (Date.now() > bot.lastSave + 3600000) // one hour
    {
        bot.saveBackup(true);
        bot.lastSave = Date.now();
    }

    for (var i = 0; i < words.length; i++)
    {
        var allMentions = words[i].match(/(<@)!?([0-9]+)(>)/g);

        if (allMentions && allMentions.length > 0)
        {
            for (var m = 0; m < allMentions.length; m++)
            {
                var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                if (validMention && validMention[2] != bot.id)
                {
                    if (group.indexOf(validMention[2]) < 0)
                    {
                        group.push(validMention[2]);
                    }
                }
            }
        }
    }

    for (var i = 0; i < group.length; i++)
    {
        if (group[i] != userID)
        {
            bot.data[id].mentions = bot.data[id].mentions || {};

            bot.data[id].mentions[group[i]] = (bot.data[id].mentions[group[i]] || 0) + 1;
        }
    }

    var mentionInfectChance = 0.25;
    var randomInfectChance = 0.998;

    bot.data[id].corona = bot.data[id].corona || [];

    var infected = false;

    for (var i = 0; i < bot.data[id].corona.length; i++)
    {
        if (bot.data[id].corona[i].id == userID)
        {
            if (bot.data[id].corona[i].time <= Date.now() - 86400000) //24 hours
            {
                bot.data[id].corona.splice(i, 1);
            }
            else
            {
                infected = true;
            }

            break;
        }
    }

    if (infected)
    {
        for (var w = 0; w < words.length; w++)
        {
            var allMentions = words[w].match(/(<@)!?([0-9]+)(>)/g);

            if (allMentions && allMentions.length > 0)
            {
                for (var m = 0; m < allMentions.length; m++)
                {
                    var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                    if (validMention && validMention[2] != bot.id)
                    {
                        var otherInfected = false;

                        for (var o = 0; o < bot.data[id].corona.length; o++)
                        {
                            if (bot.data[id].corona[o].id == validMention[2])
                            {
                                if (bot.data[id].corona[o].time <= Date.now() - 86400000) //24 hours
                                {
                                    bot.data[id].corona.splice(o, 1);
                                }
                                else
                                {
                                    otherInfected = true;

                                    if (Math.random() >= mentionInfectChance)
                                    {
                                        bot.data[id].corona[o].time = Date.now(); //re-infect, resetting timer.
                                    }
                                }

                                break;
                            }
                        }

                        if (!otherInfected) //not infected yet, so we might infect
                        {
                            if (Math.random() >= mentionInfectChance)
                            {
                                bot.data[id].corona.push({ id: validMention[2], time: Date.now() }); //Wasn't infected yet, but is now.
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (Math.random() >= randomInfectChance)
        {
            bot.data[id].corona.push({ id: userID, time: Date.now() });
        }
        else
        {
            var didInfect = false;
            for (var i = 0; i < words.length; i++)
            {
                if (didInfect)
                    break;

                var allMentions = words[i].match(/(<@)!?([0-9]+)(>)/g);

                if (allMentions && allMentions.length > 0)
                {
                    for (var m = 0; m < allMentions.length; m++)
                    {
                        var validMention = allMentions[m].match(/(<@)!?([0-9]+)(>)/);
                        if (validMention && validMention[2] != bot.id)
                        {
                            //validMention[2]
                            var otherInfected = false;

                            for (var o = 0; o < bot.data[id].corona.length; o++)
                            {
                                if (bot.data[id].corona[o].id == validMention[2])
                                {
                                    if (bot.data[id].corona[o].time <= Date.now() - 86400000) //24 hours
                                    {
                                        bot.data[id].corona.splice(o, 1);
                                        break;
                                    }
                                    else
                                    {
                                        otherInfected = true;
                                        break;
                                    }
                                }
                            }

                            if (otherInfected) //aka can infect me
                            {
                                if (Math.random() >= mentionInfectChance)
                                {
                                    bot.data[id].corona.push({ id: userID, time: Date.now() });
                                    didInfect = true;
                                    infected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (infected)
    {
        bot.addReaction({
            channelID: channelID,
            messageID: evt.d.id,
            reaction: ":corona:686267918442299459"
        });
    }

    if (message.substring(0, 1) == '.') 
    {
        var args = message.substring(1).split(' ');
        var cmd = args[0];

        var info = { user: user, userID: userID, channelID: channelID, message: message, evt: evt, serverId: id };

        bot.data.lastServer = channelID;

        args = args.splice(1);
        for (var i = 0; i < commands.length; i++)
        {
            if (cmd.toLowerCase() == commands[i].cmd)
            {
                if (commands[i].hidden && admins.indexOf(info.userID) == -1)
                    return;

                commands[i].execute(bot, info, args);
                break;
            }
        }
    }

    bot.saveData();
});

function animatedMessage (bot, channelID, messages, tickTime, messageId = null)
{
    if (messageId)
    {
        bot.editMessage(
            {
                channelID: channelID,
                messageID: messageId,
                message: messages[0],
            }, function (a, b)
        {
            messages.splice(0, 1);
            setTimeout(() =>
            {
                animatedMessage(bot, channelID, messages, tickTime, messageId);
            }, tickTime);
        });
    }
    else
    {
        bot.sendMessage({
            to: channelID,
            message: messages[0],
            typing: false
        }, function (a, b)
        {
            messages.splice(0, 1);
            setTimeout(() =>
            {
                animatedMessage(bot, channelID, messages, tickTime, b.id);
            }, tickTime);
        });
    }
}

console.log("onmessage defined");