var http = require('http');
var fs = require('fs');

if (!Object.entries)
{
    Object.entries = function (obj)
    {
        var ownProps = Object.keys(obj),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array!
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];

        return resArray;
    };
}

exports.admins =
    [
        "124136556578603009"
    ];

exports.cmds =
    [
        {
            cmd: "invitelink",
            params: "none",
            category: "main",
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "https://discordapp.com/oauth2/authorize?&client_id=1034390292570308658&scope=bot&permissions=265280",
                    typing: false
                });
            }
        },
        {
            cmd: "update",
            params: "none",
            hidden: true,
            category: "admin",
            execute: function (bot, info, args)
            {
                var exec = require('child_process').exec;

                bot.data.update = info.channelID;

                bot.sendMessage({
                    to: info.channelID,
                    message: "Fetching changes...",
                    typing: false
                }, function ()
                {
                    exec('git fetch --all', function (err, stdout, stderr) 
                    {
                        exec('git log --oneline master..origin/master', function (err, stdout, stderr) 
                        {
                            bot.sendMessage({
                                to: info.channelID,
                                message: (stdout == "" ? "No changes, " : "Changes: ```" + stdout + "``` Updating and ") + "reloading!",
                                typing: false
                            }, function ()
                            {
                                bot.suicide();
                            });
                        });
                    });
                });
            }
        },
        {
            cmd: "vaccinate",
            params: "anything",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.data[info.serverId].corona = [];

                bot.sendMessage({
                    to: info.channelID,
                    message: "Done!",
                    typing: false
                });
            }
        },
        {
            cmd: "infectlist",
            params: "anything",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                bot.sendMessage({
                    to: info.channelID,
                    message: "```Infected: " + JSON.stringify(bot.data[info.serverId].corona) + "```",
                    typing: false
                });
            }
        },
        {
            cmd: "health",
            params: "nothing",
            category: "fun",
            execute: function (bot, info, args)
            {
                var totalTime = 86400000;
                var healthFraction = 1;

                bot.data[info.serverId].corona = bot.data[info.serverId].corona || [];

                for (var i = 0; i < bot.data[info.serverId].corona.length; i++)
                {
                    if (bot.data[info.serverId].corona[i].id == info.userID)
                    {
                        healthFraction = (Date.now() - bot.data[info.serverId].corona[i].time) / totalTime;
                        break;
                    }
                }

                bot.sendMessage({
                    to: info.channelID,
                    message: "<@!" + info.userID + ">" + ' Your health: `' + Math.floor(healthFraction * 100) + "%`.",
                    typing: false
                });
            }
        },
        {
            cmd: "infect",
            params: "@mention target",
            category: "admin",
            hidden: true,
            execute: function (bot, info, args)
            {
                var validMention = args[0].match(/(<@)!?([0-9]+)(>)/);
                if (!validMention)
                {
                    bot.sendMessage({
                        to: info.channelID,
                        message: "Invalid target.",
                        typing: false
                    });
                    return;
                }

                var mention = validMention[2];

                if (validMention)
                {
                    var infected = false;

                    bot.data[info.serverId].corona = bot.data[info.serverId].corona || [];

                    for (var i = 0; i < bot.data[info.serverId].corona.length; i++)
                    {
                        if (bot.data[info.serverId].corona[i].id == mention)
                        {
                            infected = true;
                            bot.data[info.serverId].corona[i].time = Date.now();

                            bot.sendMessage({
                                to: info.channelID,
                                message: "Timer reset.",
                                typing: false
                            });

                            break;
                        }
                    }

                    if (!infected)
                    {
                        bot.data[info.serverId].corona.push({ id: validMention[2], time: Date.now() });

                        bot.sendMessage({
                            to: info.channelID,
                            message: "<@!" + validMention[2] + "> infected.",
                            typing: false
                        });
                    }
                }
            }
        }
    ];